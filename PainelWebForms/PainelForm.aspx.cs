﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PainelWebForms
{
    public partial class PainelForm : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PanelPersonalInfo.Visible = true;
                PanelAddress.Visible = false;
                PanelLogin.Visible = false;
                PanelSummary.Visible = false;
            }
        }

        private void StoreTextValues()
        {
            ViewState["FirstName"] = TextBoxFirstName.Text;
            ViewState["LastName"] = TextBoxLastName.Text;
            ViewState["Gender"] = DropDownListGender.SelectedValue;
            ViewState["Phone"] = TextBoxPhone.Text;
            ViewState["Address"] = TextBoxAddress.Text;
            ViewState["City"] = TextBoxCity.Text;
            ViewState["PostalCode"] = TextBoxPostalCode.Text;
            ViewState["Username"] = TextBoxUsername.Text;
            ViewState["Password"] = TextBoxPassword.Text;
        }

        private void RestoreTextValues()
        {
            TextBoxFirstName.Text = ViewState["FirstName"] as string ?? "";
            TextBoxLastName.Text = ViewState["LastName"] as string ?? "";
            DropDownListGender.SelectedValue = ViewState["Gender"] as string ?? "N";
            TextBoxPhone.Text = ViewState["Phone"] as string ?? "";
            TextBoxAddress.Text = ViewState["Address"] as string ?? "";
            TextBoxCity.Text = ViewState["City"] as string ?? "";
            TextBoxPostalCode.Text = ViewState["PostalCode"] as string ?? "";
            TextBoxUsername.Text = ViewState["Username"] as string ?? "";
            TextBoxPassword.Attributes.Add("value", ViewState["Password"] as string ?? "");
        }

        protected void ButtonNext_Click(object sender, EventArgs e)
        {
            StoreTextValues();
            MoveToPanel((sender as Button).CommandArgument);
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            MoveToPanel((sender as Button).CommandArgument);
            RestoreTextValues();
        }

        private void MoveToPanel(string panelId)
        {
            PanelPersonalInfo.Visible = panelId == "PanelPersonalInfo";
            PanelAddress.Visible = panelId == "PanelAddress";
            PanelLogin.Visible = panelId == "PanelLogin";
            PanelSummary.Visible = panelId == "PanelSummary";
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            StoreTextValues(); 
            ShowSummary();
        }

        private void ShowSummary()
        {
            MoveToPanel("PanelSummary");
            LabelSummary.Text = $@"
                Nome: {ViewState["FirstName"]}<br />
                Sobrenome: {ViewState["LastName"]}<br />
                Gênero: {GetGenderText(ViewState["Gender"] as string)}<br />
                Celular: {ViewState["Phone"]}<br />
                Endereço: {ViewState["Address"]}<br />
                Cidade: {ViewState["City"]}<br />
                CEP: {ViewState["PostalCode"]}<br />
                Usuário: {ViewState["Username"]}<br />";
        }

        private string GetGenderText(string genderValue)
        {
            switch (genderValue)
            {
                case "F":
                    return "Mulher";
                case "M":
                    return "Homem";
                case "N":
                    return "Não Informar";
                default:
                    return "Não Informar";
            }
        }
    }
}