﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PainelForm.aspx.cs" Inherits="PainelWebForms.PainelForm" %>

<!DOCTYPE html>
<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Atividade Painel</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <form id="form1" runat="server">

            <asp:Panel ID="PanelPersonalInfo" runat="server">
                <h2>Painel de Informações Pessoais</h2>
                <p>
                    <asp:Label ID="LabelFirstName" runat="server" Text="Nome:" />
                    <asp:TextBox ID="TextBoxFirstName" runat="server" />
                </p>
                <p>
                    <asp:Label ID="LabelLastName" runat="server" Text="Sobrenome:" />
                    <asp:TextBox ID="TextBoxLastName" runat="server" />
                </p>
                <p>
                    <asp:Label ID="LabelGender" runat="server" Text="Gênero:" />
                    <asp:DropDownList ID="DropDownListGender" runat="server">
                        <asp:ListItem Text="Mulher" Value="F"></asp:ListItem>
                        <asp:ListItem Text="Homem" Value="M"></asp:ListItem>
                        <asp:ListItem Text="Não Informar" Value="N"></asp:ListItem>
                    </asp:DropDownList>
                </p>
                <p>
                    <asp:Label ID="LabelPhone" runat="server" Text="Celular:" />
                    <asp:TextBox ID="TextBoxPhone" runat="server" />
                </p>
                <p class="button-container">

                    <asp:Button ID="ButtonNextToAddress" runat="server" Text="Próximo" class="text-white bg-green-300" OnClick="ButtonNext_Click" CommandArgument="PanelAddress" />
                </p>
            </asp:Panel>

            <asp:Panel ID="PanelAddress" runat="server">
                <h2>Painel de Endereço</h2>
                <p>
                    <asp:Label ID="LabelAddress" runat="server" Text="Endereço:" />
                    <asp:TextBox ID="TextBoxAddress" runat="server" />
                </p>
                <p>
                    <asp:Label ID="LabelCity" runat="server" Text="Cidade:" />
                    <asp:TextBox ID="TextBoxCity" runat="server" />
                </p>
                <p>
                    <asp:Label ID="LabelPostalCode" runat="server" Text="CEP:" />
                    <asp:TextBox ID="TextBoxPostalCode" runat="server" />
                </p>
                <p class="button-container">

                    <asp:Button ID="ButtonBackToPersonalInfo" runat="server" Text="Voltar" class="text-white bg-red-300" OnClick="ButtonBack_Click" CommandArgument="PanelPersonalInfo" />
                </p>
                <p class="button-container">

                    <asp:Button ID="ButtonNextToLogin" runat="server" Text="Próximo" class="text-white bg-green-300" OnClick="ButtonNext_Click" CommandArgument="PanelLogin" />
                </p>
            </asp:Panel>


            <asp:Panel ID="PanelLogin" runat="server">
                <h2>Tela de Login</h2>
                <p>
                    <asp:Label ID="LabelUsername" runat="server" Text="Usuário:" />
                    <asp:TextBox ID="TextBoxUsername" runat="server" />
                </p>
                <p class="password-field">
                    <asp:Label ID="LabelPassword" runat="server" Text="Senha:" />
                    <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" CssClass="password-field" />
                    <i id="togglePasswordIcon" class="fas fa-eye-slash field-icon" onclick="togglePasswordVisibility();"></i>
                </p>
                <p class="button-container">
                    <asp:Button ID="ButtonBackToAddress" runat="server" Text="Voltar" CssClass="button text-white bg-red-300" OnClick="ButtonBack_Click" CommandArgument="PanelAddress" />
                    <asp:Button ID="ButtonLogin" runat="server" Text="Entrar" CssClass="button text-white bg-green-500" OnClientClick="return validateLogin();" OnClick="ButtonLogin_Click" />
                </p>
            </asp:Panel>



            <asp:Panel ID="PanelSummary" runat="server">
                <h2>Resumo das Informações</h2>
                <asp:Label ID="LabelSummary" runat="server"></asp:Label>
            </asp:Panel>
        </form>
    </div>

    <script>
        function togglePasswordVisibility() {
            var passwordInput = document.getElementById('<%= TextBoxPassword.ClientID %>');
            var toggleIcon = document.getElementById('togglePasswordIcon');
            if (passwordInput.type === 'password') {
                passwordInput.type = 'text';
                toggleIcon.classList.remove('fa-eye-slash');
                toggleIcon.classList.add('fa-eye');
            } else {
                passwordInput.type = 'password';
                toggleIcon.classList.remove('fa-eye');
                toggleIcon.classList.add('fa-eye-slash');
            }
        }
        function validateLogin() {
            var username = document.getElementById('<%= TextBoxUsername.ClientID %>').value;
            var password = document.getElementById('<%= TextBoxPassword.ClientID %>').value;
            if (username === '' || password === '') {
                alert('Por favor, preencha todos os campos de login.');
                return false;
            }
            return true;
        }
 </script>



</body>
</html>
